class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.guarantee.subject
  #
  def guarantee(user, product)
    @user = user
    @product = product
    @url = 'http://example.com/login'
    mail to: user, subject: 'guarantee expiration'
  end
end

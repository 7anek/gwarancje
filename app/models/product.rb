class Product < ActiveRecord::Base
	extend FriendlyId
	friendly_id :name, use: :slugged
	has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  	belongs_to :user
  	acts_as_taggable # Alias for acts_as_taggable_on :tags
  	acts_as_taggable_on :skills, :interests
end

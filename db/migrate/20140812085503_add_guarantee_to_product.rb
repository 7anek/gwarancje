class AddGuaranteeToProduct < ActiveRecord::Migration
  def change
    add_column :products, :guarantee_expiration, :date
  end
end

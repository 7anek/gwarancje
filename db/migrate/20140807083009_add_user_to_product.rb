class AddUserToProduct < ActiveRecord::Migration
  def change
    add_reference :products, :user_id, index: true
  end
end

Rails.application.routes.draw do
  devise_for :users
  resources :main_pages

  resources :products

  root 'main_pages#index'
get 'tags/:tag', to: 'products#index', as: :tag
end
